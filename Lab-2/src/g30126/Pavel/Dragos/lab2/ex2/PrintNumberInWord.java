package g30126.Pavel.Dragos.lab2.ex2;

import java.util.Scanner;

public class PrintNumberInWord {
	  static void f1(int a)
		{	
			if(a==0)
			{
				System.out.println("ZERO");
			}
			else if(a==1)
			{
				System.out.println("ONE");
			}
			else if(a==2)
			{
				System.out.println("TWO");
			}
			else if(a==3)
			{
				System.out.println("THREE");
			}
			else if(a==4)
			{
				System.out.println("FOUR");
			}
			else if(a==5)
			{
				System.out.println("FIVE");
			}
			else if(a==6)
			{
				System.out.println("SIX");
			}
			else if(a==7)
			{
				System.out.println("SEVEN");
			}
			else if(a==8)
			{
				System.out.println("EIGHT");
			}
			else if(a==9)
			{
				System.out.println("NINE");
			}
		}
	  static void f2(int a)
	  {	
		  
		  switch(a)
		  {
		  		case 0: System.out.println("ZERO");
		  				break;
		  		case 1: System.out.println("ONE");
		  				break;
		  		case 2: System.out.println("TWO");
		  				break;
		  		case 3: System.out.println("THREE");
		  				break;
		  		case 4: System.out.println("FOUR");
		  				break;
		  		case 5: System.out.println("FIVE");
		  				break;
		  		case 6: System.out.println("SIX");
		  				break;
		  		case 7: System.out.println("SEVEN");
		  				break;
		  		case 8: System.out.println("EIGHT");
		  				break;
		  		case 9: System.out.println("NINE");
		  				break;
		  		default: System.out.println("ERROR");
		  				break;
		  }
	  }
	public static void main(String[] args) {
		Scanner in= new Scanner (System.in);
		int a=in.nextInt();
		f1(a);
		f2(a);
	}

}
