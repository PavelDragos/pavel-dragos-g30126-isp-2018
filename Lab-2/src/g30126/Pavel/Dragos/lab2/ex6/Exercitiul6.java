package g30126.Pavel.Dragos.lab2.ex6;

import java.util.Scanner;

public class Exercitiul6 {
	static int recursiv (int N)
	{	if(N==0)
		{
			return 1;
		}
		else
		{
			return N*recursiv(N-1);
		}
	}
	static int nerecursiv (int N)
	{	
		int factorial=1;
		while(N>0)
		{
			factorial*=N;
			N--;
		}
		return factorial;
	}
	public static void main(String[] args) {
		
		Scanner in=new Scanner(System.in);
		int N;
		N=in.nextInt();
		int result,result1;
		result=recursiv(N);
		result1=nerecursiv(N);
		System.out.println(result+" "+result1);

	}

}
