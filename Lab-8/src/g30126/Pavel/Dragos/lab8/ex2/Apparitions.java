package g30126.Pavel.Dragos.lab8.ex2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Apparitions {
	private String filename;
	private char character;

	public Apparitions() {
		char c;
		Scanner s = new Scanner(System.in);
		c = s.next().charAt(0);
		this.character = c;
		filename = "D:\\\\Facultate\\\\An 2\\\\Semestrul 2\\\\ISP\\\\Lab-8\\\\data.txt.txt ";
	}

	public int numberOfApparitions() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(filename));
		String s, s1 = new String();
		while ((s = in.readLine()) != null)
			s1 += s + "\n";
		in.close();
		System.out.println(s1);
		int counter = 0;
		for (int i = 0; i < s1.length(); i++) {
			char c = s1.charAt(i);
			if (c == character)
				counter++;
		}
		System.out.println(counter);
		return counter;
	}

	public static void main(String[] args) throws IOException {
		Apparitions a = new Apparitions();
		System.out.println(a.numberOfApparitions());
	}

}
