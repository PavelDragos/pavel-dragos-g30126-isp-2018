package g30126.Pavel.Dragos.lab8.ex4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;

public class Car implements Serializable {
	private String model;
	private double price;

	public Car() {

	}

	public Car(String model, double price) {
		this.model = model;
		this.price = price;
	}

	public String toString() {
		return "Masina " + model + " are pretul " + price + "\n";
	}

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		Car c1 = new Car("VW", 3200);
		Car c2 = new Car("BMW", 5000);
		Car c3 = new Car("Toyota", 6000);
		Car c4 = new Car("Ford", 300);
		LinkedList<Car> cars = new LinkedList<Car>();
		cars.addLast(c1);
		cars.addLast(c2);
		cars.addLast(c3);
		cars.addLast(c4);
		Iterator<Car> i = cars.iterator();
		try {
			ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("Cars.txt"));
			while (i.hasNext()) {
				Car c = (Car) i.next();
				o.writeObject(c);
			}
			o.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
		try {
			ObjectInputStream in = new ObjectInputStream(
					new FileInputStream("D:\\Facultate\\An 2\\Semestrul 2\\ISP\\Lab-8\\Cars.txt"));
			Iterator<Car> j = cars.iterator();
			while (j.hasNext() && j.next() != null) {
				Car c = (Car) in.readObject();
				System.out.println(c);
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
		try {
			ObjectInputStream in = new ObjectInputStream(
					new FileInputStream("D:\\Facultate\\An 2\\Semestrul 2\\ISP\\Lab-8\\Cars.txt"));
			Car c = (Car) in.readObject();
			System.out.println(c);
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
	}

}
