package g30126.Pavel.Dragos.lab10.ex3;

public class DoubleCounters extends Thread {
	Thread t;
	String name;
	int start;
	int stop;

	public DoubleCounters(String name, Thread t, int start, int stop) {
		this.name = name;
		this.t = t;
		this.start = start;
		this.stop = stop;
	}

	public void run() {
		try {
			if (t != null)
				t.join();
			for (int i = start; i < stop; i++) {
				System.out.println("Firul" + name + " " + i);
				Thread.sleep(50);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		DoubleCounters c1 = new DoubleCounters("Counter1 ", null, 0, 101);
		DoubleCounters c2 = new DoubleCounters("Counter2 ", c1, 100, 201);
		c1.start();
		c2.start();
	}
}