package g30126.Pavel.Dragos.lab10.ex6;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class ChronometerInterface extends JFrame {
	JButton jb1;
	JButton jb2;
	JTextField jt;
	public ChronometerInterface() {
		setTitle("Chronometer");
		setSize(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initialize();
		setVisible(true);
	}
	public void initialize() {
		this.setLayout(null);
		jb1 = new JButton("Start/Stop");
		jb1.setBounds(40, 130, 100, 30);
		add(jb1);
		jb2 = new JButton("Reset");
		jb2.setBounds(141, 130, 100, 30);
		add(jb2);
		jt = new JTextField("00 : 00 : 000");
		jt.setBounds(40,99, 202, 30);
		add(jt);
	}
	
	
	public JButton getJb1() {
		return jb1;
	}
	public JButton getJb2() {
		return jb2;
	}
	public JTextField getJt() {
		return jt;
	}
}
