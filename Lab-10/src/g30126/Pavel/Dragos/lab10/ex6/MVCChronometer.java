package g30126.Pavel.Dragos.lab10.ex6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class MVCChronometer implements ActionListener,Observer {
	Chronometer c; 
	ChronometerInterface ci;
	public MVCChronometer(Chronometer c, ChronometerInterface ci) {
		this.c = c;
		this.ci = ci;
		ci.getJb1().addActionListener(this);
		ci.getJb2().addActionListener(this);
		c.addObserver(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(ci.jb1))
			c.changeState();
		else if (e.getSource().equals(ci.jb2)) {
			c.reset();
		}
	}

	@Override
	public void update(Observable obs, Object o) {
		ci.getJt().setText(o.toString());
		
	}

}
