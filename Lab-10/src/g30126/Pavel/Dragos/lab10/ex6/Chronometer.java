package g30126.Pavel.Dragos.lab10.ex6;

import java.util.Observable;

public class Chronometer extends Observable implements Runnable{
	static int miliseconds;
	static int seconds;
	static int minutes;
	boolean state ;
	Integer objSync;
	public Chronometer () {
		miliseconds = 0;
		seconds = 0;
		minutes=0;
		state = false;
		objSync = new Integer(0);
		new Thread(this).start();
	}
	public void run() {
		
		while(true) {
			if(!state) {
				synchronized(objSync) {
					try {
						objSync.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			else {
				synchronized(this) {
					this.notify();
				}
			}
			try {
				Thread.sleep(1);
				miliseconds++;
				if(miliseconds==1000) {
					seconds++;
					miliseconds=0;
				}
				if(seconds == 60) {
					miliseconds=0;
					seconds=0;
					minutes++;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.notifyObservers(minutes+" : "+seconds+" : " + miliseconds);
			this.setChanged();
			
		}
	}
	public void changeState() {
		state  = !state;
		if(state) {
			synchronized(objSync) {
				objSync.notify();
			}
		}
	}
	public void reset() {
		miliseconds = 0;
		seconds = 0;
		minutes=0;
		this.notifyObservers(0 + " : "+ 0 + " : "+ 0);
	}
}
