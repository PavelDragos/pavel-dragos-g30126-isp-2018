package g30126.Pavel.Dragos.lab7.ex2;


public class BankAccount implements Comparable{
	private String owner;
	private double balance;
	public BankAccount() {
		
	}
	public BankAccount(String owner, double balance) {
		this.owner=owner;
		this.balance=balance;
	}
	public void withdraw(double amount) {
		this.balance-=amount;
	}
	public void deposit (double amount) {
		this.balance+=amount;
	}
	public double getBalance() {
		return this.balance;
	}
	public String getOwner() {
		return this.owner;
	}
	public boolean equals(Object obj){
		if(obj instanceof BankAccount){
			BankAccount b = (BankAccount)obj;
			return b.owner.equals(owner)&& b.balance==balance;
		}
		return false;
	}
	@Override
	public int compareTo(Object o) {
		return (int)(this.balance-((BankAccount) o).getBalance());
	}
	

}
