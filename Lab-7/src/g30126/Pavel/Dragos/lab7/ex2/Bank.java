package g30126.Pavel.Dragos.lab7.ex2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;


public class Bank {
	 ArrayList<BankAccount> ba = new ArrayList<BankAccount>();
	public void addAccounts(String owner, double balance) {
		BankAccount b1 = new BankAccount(owner,balance);
		ba.add(b1);
	}
	public void printsAccount() {
		
		Collections.sort(ba);
		for(BankAccount counter: ba){
			System.out.println(counter.getOwner()+" "+counter.getBalance());
		}
	}
	public void printsAccount1(double minBalance, double maxBalance) {
		
		for(BankAccount counter: ba){
			if(counter.getBalance()>minBalance && counter.getBalance()<maxBalance)
			System.out.println(counter.getOwner()+" "+counter.getBalance());
		}
		
	}
	public BankAccount getAccount(String owner) {
		for(BankAccount counter : ba)
				if(counter.getOwner().equals(owner))
					return counter;
		return null;
	}
	public void getAllAccounts() {
		Collections.sort(ba,new OwnerComparator());
		for(BankAccount counter : ba)
		{
			System.out.println(counter.getOwner());
		}
	}
	public static void main(String[] args) {
	
		Bank b =new Bank();
		b.addAccounts("ba", 400);
		b.addAccounts("ccc", 300);
		b.addAccounts("a", 200);
		b.addAccounts("ab", 600);
		b.addAccounts("Ioanaa", 800);
		b.printsAccount();
		System.out.println();
		b.printsAccount1(300,800);
		System.out.println();
		System.out.println();
		b.getAllAccounts();
	
	}

}


