package g30126.Pavel.Dragos.lab7.ex3;

import java.util.Collections;
import java.util.TreeSet;
import java.util.Comparator;

public class Bank {
	TreeSet<BankAccount> ts1 = new TreeSet<BankAccount>(new OwnerComparator());
	TreeSet<BankAccount> ts = new TreeSet<BankAccount>();
	
	public void addAccount(String owner, double balance) {
		BankAccount b = new BankAccount(owner,balance);
		ts.add(b);
		ts1.add(b);
	}
	public void printAccounts() {
		for(BankAccount counter : ts)
			System.out.println(counter.getOwner()+" "+counter.getBalance());
	}
	public void printAccounts(double minBalance, double maxBalance){
		for(BankAccount counter : ts)
		{	
			if(counter.getBalance()>minBalance && counter.getBalance()<maxBalance)
				System.out.println(counter.getOwner()+" "+counter.getBalance());
		}
	}
	public BankAccount getAccount (String owner) {
		for(BankAccount counter : ts)
		{	if(counter.getOwner().equals(owner))
				return counter;
		}
		return null;
	}
	public void getAllAccounts() {
		for(BankAccount counter : ts1)
			System.out.println(counter.getOwner()+" "+counter.getBalance());
	}
	
	public static void main(String[] args) {
		Bank b = new Bank();
		b.addAccount("ba", 400);
		b.addAccount("ccc", 300);
		b.addAccount("a", 200);
		b.addAccount("ab", 600);
		b.addAccount("Ioanaa", 800);
		b.printAccounts();
		System.out.println();
		System.out.println();
		b.getAllAccounts();
	}
}
