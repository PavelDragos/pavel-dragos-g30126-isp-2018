package g30126.Pavel.Dragos.lab7.ex3;

public class BankAccount implements Comparable {
	private String owner;
	private double balance;
	public BankAccount(String owner, double balance) {
		this.owner=owner;
		this.balance=balance;
	}
	public void withdraw(double amount) {
		balance-=amount;
	}
	public void deposit(double amount) {
		balance+=amount;
	}
	public String getOwner() {
		return owner;
	}
	public double getBalance() {
		return balance;
	}
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount) {
			BankAccount b = (BankAccount)obj;
		return b.owner.equals(owner) && b.balance==balance;
		}
		return false;
	}
	@Override
	public int compareTo(Object o) {
		return (int)(this.balance-((BankAccount)o).getBalance());
		
	}
}
