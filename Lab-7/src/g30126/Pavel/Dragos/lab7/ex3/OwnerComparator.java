package g30126.Pavel.Dragos.lab7.ex3;

import java.util.Comparator;

public class OwnerComparator implements Comparator<BankAccount>{

	@Override
	public int compare(BankAccount o1, BankAccount o2) {
		String ba1 = o1.getOwner().toUpperCase();
		String ba2 = o2.getOwner().toUpperCase();
		return ba1.compareTo(ba2);
		
	}

}
