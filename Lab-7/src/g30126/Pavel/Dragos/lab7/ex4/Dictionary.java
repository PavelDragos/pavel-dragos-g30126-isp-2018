package g30126.Pavel.Dragos.lab7.ex4;

import java.util.HashMap;

public class Dictionary {
	private HashMap<Word,Definition> hm = new HashMap<Word,Definition>();
	public Dictionary() {
		
	}
	public void addWord(Word w, Definition d) {
		if(!(hm.containsKey(w)))
			hm.put(w, d);
	}
	public Definition getDefinition(Word w) {
		
		if(hm.containsKey(w))
		{	
			//System.out.println(hm.get(w).getDescription());
			return hm.get(w);
		}
		return null;
	}
	public void getAllWords() {
		for(Word name : hm.keySet())
		{
			System.out.println(name.getName());
		}
		
	}
	public void getAllDefinitions() {
		for(Definition d : hm.values())
			System.out.println(d.getDescription());
	}
	
	
	public static void main(String[] args) {
			Dictionary d = new Dictionary();
			Word w1 = new Word("magar");
			Definition d1 = new Definition("patruped");
			Word w2 = new Word("om");
			Definition d2 = new Definition("biped");
			d.addWord(w1, d1);
			d.addWord(w2, d2);
			d.getAllWords();
			d.getAllDefinitions();
			System.out.println();
			System.out.println(d.getDefinition(w1).getDescription());
	}

}
