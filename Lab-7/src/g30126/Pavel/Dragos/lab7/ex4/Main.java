package g30126.Pavel.Dragos.lab7.ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException {
		Dictionary d = new Dictionary();
		char raspuns;
		String linie, explic;
		BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
		do {
			System.out.println("Meniu");
			System.out.println("a-Adauga cuvant");
			System.out.println("c-Cauta cuvant");
			System.out.println("l-Listeaza  dictionar dupa cuvant");
			System.out.println("d-Listeaza dictionar dupa definitie");
			System.out.println("e-Iesi");
			linie = fluxIn.readLine();
			raspuns = linie.charAt(0);
			switch(raspuns) {
				case 'a':
						System.out.println("Introduceti cuvant: ");
						linie = fluxIn.readLine();
						if(linie.length()>1)
							{	
								Word w = new Word(linie);
								System.out.println("Introduceti definitia");
								explic = fluxIn.readLine();
								Definition def = new Definition(explic);
								d.addWord(w,def);
							}	
						break;
				case 'c':
						System.out.println("Dati cuvantul a carui definitie o cautati.");
						linie=fluxIn.readLine();
						Word w2 = new Word(linie);
						Definition def = new Definition();
						//System.out.println(w2.getName());
						def=d.getDefinition(w2);
						System.out.println(def.getDescription());
						break;
				case 'l':
						System.out.println("Afiseaza toate cuvintele dictionarului.");
						d.getAllWords();
						break;
				case 'd':
						System.out.println("Afiseaza toate definitiile dictionarului.");
						d.getAllDefinitions();
			}
		}while(raspuns!='e');
				System.out.println("Program terminat");
							
		}
	}

