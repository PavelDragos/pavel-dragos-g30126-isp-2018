package g30126.Pavel.Dragos.lab7.ex4;

public class Word {
	private String name;
	public Word() {
		
	}
	public Word(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;	
	}
}
