package g30126.Pavel.Dragos.lab7.ex4;

public class Definition {
	private String description;
	public Definition() {
		
	}
	public Definition(String description) {
		this.description=description;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description=description;
	}
}
