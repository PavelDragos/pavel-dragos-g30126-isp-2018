package g30126.Pavel.Dragos.lab7.ex1;

public class BankAccount {
	private String owner;
	private double balance;
	public BankAccount(String owner, double balance) {
		this.owner=owner;
		this.balance=balance;
	}
	public void withdraw(double amount) {
		this.balance-=amount;
	}
	public void deposit (double amount) {
		this.balance+=amount;
	}
	public boolean equals(Object obj){
		if(obj instanceof BankAccount){
			BankAccount b = (BankAccount)obj;
			return b.owner.equals(owner)&& b.balance==balance;
		}
		return false;
	}
	public int hashCode() {
		return (int)balance+owner.hashCode();
	}
	public static void main(String[] args) {
		BankAccount b = new BankAccount("Vasile",1000);
		BankAccount b1 = new BankAccount("Ion",2000);
		BankAccount b2 = new BankAccount("Maria",3000);
		BankAccount b3 = new BankAccount("Darius",4000);
		BankAccount b4 = new BankAccount("Vasile",1000);
		if(b.equals(b3)) {
			System.out.println("Sunt egale.");
		}
		else {
			System.out.println("Nu sunt egale.");
		}
	}

}
