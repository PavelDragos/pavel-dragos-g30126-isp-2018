package g30126.Pavel.Dragos.lab4.ex9;
import becker.robots.*;
public class Diver {
	private int x;
	private int y;
	private City Rome; //= new City();
	private Robot karel;// = new Robot(Rome,x,y,Direction.NORTH);
	private Wall Ave0;
	private Wall Ave1;
	private Wall Ave2;
	private Wall Ave3;
	private Wall Ave4;
	private Wall Ave5;
	private Wall Ave6;
	private Wall Ave7;
	private Wall Ave8;
	public Diver(int x, int y)
		{
			this.x=x;
			this.y=y;
			Rome = new City();
			karel = new Robot(Rome,x,y,Direction.NORTH);
			Ave0 = new Wall(Rome,x+1,y,Direction.NORTH);
			Ave1 = new Wall(Rome,x+1,y-1,Direction.EAST);
			Ave2 = new Wall(Rome,x+2,y-1,Direction.EAST);
			Ave3 = new Wall(Rome,x+3,y-1,Direction.EAST);
			Ave4 = new Wall(Rome,x+3,y,Direction.WEST);
			Ave5 = new Wall(Rome,x+3,y,Direction.SOUTH);
			Ave6 = new Wall(Rome,x+3,y+1,Direction.SOUTH);
			Ave7 = new Wall(Rome,x+3,y+1,Direction.EAST);
		}
	public void Dive()
		{
			karel.move();
			karel.turnLeft();
			karel.turnLeft();
			karel.turnLeft();
			karel.move();
			karel.turnLeft();
			karel.turnLeft();
			karel.turnLeft();
			karel.move();
			karel.move();
			karel.move();
			karel.move();
			karel.turnLeft();
			karel.turnLeft();
		}
	public static void main(String[] args) {
		Diver d = new Diver(2,2);
		d.Dive();

	}

}
