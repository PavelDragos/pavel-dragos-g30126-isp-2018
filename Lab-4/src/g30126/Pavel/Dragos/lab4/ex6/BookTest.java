package g30126.Pavel.Dragos.lab4.ex6;

import static org.junit.Assert.*;

import org.junit.Test;

import g30126.Pavel.Dragos.lab4.ex4.Author;

public class BookTest {

	@Test
	public void testToString() {
		Author[] a;
		Book b = new Book("Piciul",a= new Author[3],10.15);
		a[0] = new Author("Ion","Ion@yahoo.com",'m');
		a[1] = new Author("Vasile","Vasile@gmail.com",'m');
		a[2] = new Author("Ion","Ion@yahoo.com",'m');
		assertEquals("book-Piciul by 3 authors",b.toString());
	}
	
	@Test
	public void testPrintAuthors() {
		Author[] a;
		Book b = new Book("Piciul",a= new Author[3],10.15);
		a[0] = new Author("Ion","Ion@yahoo.com",'m');
		a[1] = new Author("Maria","Maria@gmail.com",'f');
		a[2] = new Author("Gheorghe","ghg@yahoo.com",'m');
		assertEquals("Ion",a[0].getName());
		assertEquals("Maria",a[1].getName());
		assertEquals("Gheorghe",a[2].getName());
		
	}

}
