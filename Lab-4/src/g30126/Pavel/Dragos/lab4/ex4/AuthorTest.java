package g30126.Pavel.Dragos.lab4.ex4;

import static org.junit.Assert.*;

import org.junit.Test;

public class AuthorTest {

	@Test
	public void testToString() {
		Author a = new Author("Ion","ion@gmail.com",'m');
		assertEquals("author-Ion (m) at ion@gmail.com",a.toString());
	}

}
