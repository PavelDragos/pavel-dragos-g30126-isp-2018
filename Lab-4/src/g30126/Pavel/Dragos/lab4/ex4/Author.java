package g30126.Pavel.Dragos.lab4.ex4;

public class Author {
	private String name;
	private String email;
	private char gender;
	public Author(String name, String email, char gender)
	{
		this.name = name;
		this.email = email;
		this.gender = gender;
	}
	public String getName ()
	{
		return name;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{	
		this.email=email;
	}
	public char getGender()
	{
		return gender;
	}
	public String toString ()
	{
		String message="author-"+name+" ("+gender+")"+" at "+email;
		return message;
	}
	public static void main(String[] args) {
		

	}

}
