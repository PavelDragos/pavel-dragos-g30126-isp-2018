package g30126.Pavel.Dragos.lab4.ex8;

import static org.junit.Assert.*;

import org.junit.Test;

public class SquareTest {

	@Test
	public void testToString() {
		Square s = new Square();
		assertEquals("A Square with side=1.0, which is a subclass of A Rectangle with width=1.0 and length=1.0, which is a subclass of A Shape with color of green and filled true",s.toString());
	}

}
