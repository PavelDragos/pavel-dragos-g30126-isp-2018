package g30126.Pavel.Dragos.lab4.ex8;

import static org.junit.Assert.*;

import org.junit.Test;

public class CircleTest {

	@Test
	public void testToString() {
		Circle c = new Circle();
		assertEquals("A Circle with radius=1.0, which is a subclass of A Shape with color of green and filled true",c.toString());
	}
	 
	@Test
	public void testGetArea() {
		Circle c  = new Circle();
		assertEquals(Math.PI,c.getArea(),0.01);
	}

	@Test
	public void testGetPerimeter() {
		Circle c = new Circle();
		assertEquals(2*Math.PI,c.getPerimeter(),0.01);
	}

}
