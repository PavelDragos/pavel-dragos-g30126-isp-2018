package g30126.Pavel.Dragos.lab4.ex5;

import static org.junit.Assert.*;

import org.junit.Test;

import g30126.Pavel.Dragos.lab4.ex4.Author;

public class BookTest {

	@Test
	public void testToString() {
		Author a = new Author("Roald Dahl","RoaldDahl@gmail.com",'m');
		Book b = new Book("Charlie si fabrica de ciocolata",a,20.99);
		assertEquals("'book-Charlie si fabrica de ciocolata' by author-Roald Dahl (m) at RoaldDahl@gmail.com",b.toString());
		
	}

}
