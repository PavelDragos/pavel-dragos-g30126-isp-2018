package g30126.Pavel.Dragos.lab4.ex3;

import static org.junit.Assert.*;

import org.junit.Test;

public class CircleTest {

	@Test
	public void testGetRadius() {
		Circle c = new Circle();
		assertEquals(1.0,c.getRadius(),0.01);
	}

	@Test
	public void testGetArea() {
		Circle c = new Circle();
		assertEquals(Math.PI,c.getArea(),0.01);
	}
}

