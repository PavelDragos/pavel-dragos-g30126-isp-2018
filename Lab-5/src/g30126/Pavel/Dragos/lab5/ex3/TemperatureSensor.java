package g30126.Pavel.Dragos.lab5.ex3;

public class TemperatureSensor extends Sensor {
	
	int valueT;
	public TemperatureSensor() {
		this.valueT=(int)(Math.random()*100);
	}
	public int readValue() {
		return this.valueT;
	}
}
