package g30126.Pavel.Dragos.lab5.ex1;

public class Square extends Rectangle{
	public Square()
		{
			super();
		}
	public Square(double side)
		{
			super(side,side);
		}
	public Square(double side, String color, Boolean filled)
		{
			super(side,side,color,filled);
		}
	public double getSide()
		{
			return getLength();
		}
	public void setSide(double side)
		{
			super.setLength(side);
			super.setWidth(side);
		}
	public void setWidth(double side)
		{
			super.setWidth(side);
		}
	public void setLength(double side)
		{
			super.setLength(side);
		}
		@Override
		   public String toString() {
		       return "Rectangle{" +
		               "color='" + color + '\'' +
		               ", filled=" + filled +
		               ", width=" + width + ", length="+ length+
		               '}';
		}
}