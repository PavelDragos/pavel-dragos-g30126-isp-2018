package g30126.Pavel.Dragos.lab3.ex5;
import becker.robots.*;
public class Exercitiul5 {

	public static void main(String[] args) {
		City Kiev = new City();
		Wall blockAve01 = new Wall (Kiev,1,1,Direction.NORTH);
		Wall blockAve02 = new Wall (Kiev,1,2,Direction.NORTH);
		Wall BlockAve03 = new Wall (Kiev,1,2,Direction.EAST);
		Wall blockAve04 = new Wall (Kiev,1,2,Direction.SOUTH);
		Wall blockAve05 = new Wall (Kiev,1,1,Direction.WEST);
		Wall blockAve06 = new Wall (Kiev,2,1,Direction.WEST);
		Wall blockAve07 = new Wall (Kiev,2,1,Direction.SOUTH);
		Robot karel = new Robot(Kiev,1,2,Direction.SOUTH);
		Thing circle = new Thing(Kiev,2,2);
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.pickThing();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
	}

}
