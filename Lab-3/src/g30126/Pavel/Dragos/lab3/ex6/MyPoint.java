package g30126.Pavel.Dragos.lab3.ex6;
import java.lang.*;

public class MyPoint{
	private int x;
	private int y;
	MyPoint()
	{
		x=0;
		y=0;
	}
	MyPoint(int x, int y)
	{
		this.x=x;
		this.y=y;
	}
	 int getX ()
	{
		return x;
	}
	int getY ()
	{
		return y;
	}
	void setX (int x)
	{
		this.x=x;
	}
	void setY(int y)
	{
		this.y=y;
	}
	void setXY (int x, int y)
	{
		this.x=x;
		this.y=y;
	}
	public String toString()
	{
		/*
		 * StringBuffer message = new StringBuffer();
		message.append("(").append(getX()).append(",").append(getY()).append(")");
		return message.toString();
		*/
		String message;
		message="("+getX()+","+getY()+")";
		return message;
	}
	double distance (int x, int y)
	{
		double result=Math.sqrt((getX()-x)*(getX()-x)+(getY()-y)*(getY()-y));
		return result;
	}
	
	double distance ( MyPoint point)
	{
		double result= Math.sqrt((getX()-point.getX())*(getX()-point.getX())+(getY()-point.getY())*(getY()-point.getY()));
		return result;	
	}
	



	public static void main(String[] args) {
		
		MyPoint p0 = new MyPoint(1,1);
		MyPoint p1 = new MyPoint();
		int a,b;
		double c,d;
		String text;
		a=p0.getX();
		b=p0.getY();
		System.out.println("("+a+","+b+")");
		text=p0.toString();
		System.out.println(text);
		p0.setX(2);
		p0.setY(2);
		p1.setXY(3, 3);
		c=p0.distance(3,3);
		d=p0.distance(p1);
		System.out.println(c);
		System.out.println(d);
	}

}

		 
