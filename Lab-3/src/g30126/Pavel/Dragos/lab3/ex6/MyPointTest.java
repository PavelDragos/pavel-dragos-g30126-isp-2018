package g30126.Pavel.Dragos.lab3.ex6;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyPointTest {
	private MyPoint p0 = new MyPoint(1,1);
	private MyPoint p1 = new MyPoint(); 
	@Test
	public void testMyPoint() {
		
		System.out.println("Executing test 1");
		assertEquals(p1.getX(),0);
		assertEquals(p1.getY(),0);
	}

	@Test
	public void testMyPointIntInt() {
		System.out.println("Executing test 2");
		assertEquals(p0.getX(),1);
		assertEquals(p0.getY(),1);
	
	}

	@Test
	public void testGetX() {
		System.out.println("Executing test 3");
		assertEquals(p0.getX(),1);
	}

	@Test
	public void testGetY() {
		System.out.println("Executing test 4");
		assertEquals(p0.getY(),1);
	}

	@Test
	public void testSetX() {
		System.out.println("Executing test 5");
		p1.setX(2);
		assertEquals(p1.getX(),2);
	}

	@Test
	public void testSetY() {
		System.out.println("Executing test 6");
		p1.setY(2);
		assertEquals(p1.getY(),2);
	}

	@Test
	public void testSetXY() {
		System.out.println("Executing test 7");
		p0.setXY(4, 4);
		assertEquals(p0.getX(),4);
		assertEquals(p0.getY(),4);
	}

	@Test
	public void testToString() {
		System.out.println("Executing test 8");
		System.out.println(p1.toString());
		
	}

	@Test
	public void testDistanceIntInt() {
		System.out.println("Executing test 9");
		p1.setXY(4, 4);
		assertTrue(p1.distance(4,4)==0);
	}

	@Test
	public void testDistanceMyPoint() {
		System.out.println("Executing test 10");
		p0.setXY(4, 4);
		p1.setXY(4, 4);
		assertTrue(p1.distance(p0)==0);
	}

}
