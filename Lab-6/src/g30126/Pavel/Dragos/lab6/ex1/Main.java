package g30126.Pavel.Dragos.lab6.ex1;

import java.awt.*;


public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED,100,200,"Circle", 90,true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN,5,5,"Circle", 100,false);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE,30,40,"Rectangle",100,true);
        b1.addShape(s3);
        //b1.deleteById("Circle");
        		
    }
}
