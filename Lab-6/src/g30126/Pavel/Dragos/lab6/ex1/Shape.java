package g30126.Pavel.Dragos.lab6.ex1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x;
    private int y;
    private String id;
    Boolean fill;
    public Shape(Color color, int x, int y,String id,Boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }
    public Boolean getFill() {
    	return fill;
    }
    public String getId() {
    	return id;
    }
    public int getX ( )
    {
    	return x;
    }
    public	int  getY ()
    {
    	return y;
    }
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
