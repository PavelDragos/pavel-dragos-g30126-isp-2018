package g30126.Pavel.Dragos.lab6.ex1;


import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color,int x, int y,String id, int length,Boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(super.getX(),super.getY(),length,length);
        if(super.getFill()==true)
        	g.fillRect(super.getX(),super.getY(),length,length);
    }
}
