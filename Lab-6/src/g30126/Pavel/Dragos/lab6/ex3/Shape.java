package g30126.Pavel.Dragos.lab6.ex3;

import java.awt.Graphics;

interface  Shape {
	
	public abstract void draw(Graphics g);
	 public String getId();
}
