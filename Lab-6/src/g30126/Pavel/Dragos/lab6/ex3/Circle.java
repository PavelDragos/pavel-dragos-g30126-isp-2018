package g30126.Pavel.Dragos.lab6.ex3;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape {
    
	private int radius;
	private Color color;
	private int x;
	private int y;
	private String id;
	private Boolean fill;
	
    public Circle(Color color,int x, int y,String id, int radius,Boolean fill) {
    	this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
        this.radius = radius;
    }
    
    public int getRadius() {
        return radius;
    }
    public Boolean getFill() {
    	return fill;
    }
    public String getId() {
    	return id;
    }
    public int getX ( )
    {
    	return x;
    }
    public	int  getY ()
    {
    	return y;
    }
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(getFill()==true)
        {
        	g.fillOval(getX(), getY(),radius,radius);
        }
    }
}