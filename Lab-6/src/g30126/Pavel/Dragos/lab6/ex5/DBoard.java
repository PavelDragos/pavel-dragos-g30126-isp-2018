package g30126.Pavel.Dragos.lab6.ex5;
	import javax.swing.*;

import g30126.Pavel.Dragos.lab6.ex1.Shape;

import java.awt.*;
	import java.util.ArrayList;

	public class DBoard  extends JFrame {
		
	    Rectangle[] shapes = new Rectangle[100];
	    //ArrayList<Shape> shapes = new ArrayList<>();
	    public  static final int x=700;
	    public static final int y=800;
	    public DBoard() {
	        super();
	        this.setTitle("Pyramid");
	        this.setSize(x,y);
	        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	        this.setVisible(true);
	    }
	    public void addRectangle(Rectangle s1){
	        for(int i=0;i<shapes.length;i++){
	            if(shapes[i]==null){
	                shapes[i] = s1;
	                break;
	            }
	        }
//	        shapes.add(s1);
	        this.repaint();
	    }

	    public void paint(Graphics g){
	        for(int i=0;i<shapes.length;i++){
	            if(shapes[i]!=null)
	                shapes[i].draw(g);
	        }
	    }
	    }
