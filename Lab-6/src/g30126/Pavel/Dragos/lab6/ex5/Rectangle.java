package g30126.Pavel.Dragos.lab6.ex5;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

public class Rectangle extends JFrame {
	private int nrb;
	private int width;
	private int height;
	public Rectangle() {
		
	}
    public Rectangle(int nrb, int width, int height) {
    	this.nrb=nrb;
        this.width = width;
        this.height=height;
    }

    public void draw(Graphics g) {
        int s=0;
        int nrt=0;
        int p,poz;
        while(nrt+s<nrb) {
        	s++;
        	p=s;
        	nrt+=s;
        	while(p>0) {
        		for(int i=0 ; i<p;i++)
        			for(int j=0 ; j<=i;j++)
        			{	
        				g.drawRect((DBoard.x/2-(p-j*2)*width/2), height*p+40, width, height);
        			}
        		p--;
        	}
        }
        
    }
}
