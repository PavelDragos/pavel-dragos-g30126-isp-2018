package g30126.Pavel.Dragos.lab6.ex4;

public class Ex4 implements CharSequence{
	private char[] x={'a','b','c','d','e'};
	private int n;
	@Override
	public int length() {
		return x.length;
	}

	@Override
	public char charAt(int index) {
		return x[index];
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		String s = new String();
		for(int i=start;i<end;i++)
			s=s+x[i];
		return s;
	}
	public static void main (String[] args) {
		/*char[] x = new char[100];
		for(int i=0;i<x.length;i++)
			x[i]="a"
			*/
		Ex4 e1 = new Ex4();
		//e1.x= {'a','b','c','d','e'};
		System.out.println(e1.length());
		System.out.println(e1.charAt(3));
		System.out.println(e1.subSequence(1, 4));
	}

}
