package g30126.Pavel.Dragos.lab6.ex4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Ex4Test {

	@Test
	void testLength() {
		Ex4 e1 = new Ex4();
		assertEquals(5,e1.length());
	}

	@Test
	void testCharAt() {
		Ex4 e1 = new Ex4();
		assertEquals('c',e1.charAt(2));
	}

	@Test
	void testSubSequence() {
		Ex4 e1 = new Ex4();
		assertEquals("bcd",e1.subSequence(1, 4));
	}

}
