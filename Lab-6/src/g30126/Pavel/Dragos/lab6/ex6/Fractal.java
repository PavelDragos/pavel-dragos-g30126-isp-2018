package g30126.Pavel.Dragos.lab6.ex6;
import java.awt.Graphics;

import javax.swing.JFrame;

public class Fractal extends JFrame {
		private int length;
		
		public Fractal(int length) {
			this.length=length;
		}
			public int getLength() {
				return this.length;
			}
			public void draw(Graphics g) {
	
				if(length<300)
				{	
					g.drawRect(DrawinBoard.x/2-(length/2), DrawinBoard.y/2-(length/2), length, length);
					length+=20;
					draw(g);
				}
	    }
	    
	public static void main(String[] args) {
				DrawinBoard d = new DrawinBoard();
				Fractal f = new Fractal(20);
				d.addFractal(f);
		
	}

}
