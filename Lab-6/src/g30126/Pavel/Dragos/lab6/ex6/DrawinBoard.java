package g30126.Pavel.Dragos.lab6.ex6;
import javax.swing.*;


import java.awt.*;
import java.util.ArrayList;

public class DrawinBoard  extends JFrame {
	
	Fractal[] shapes = new Fractal[100];
    //ArrayList<Shape> shapes = new ArrayList<>();
    public  static final int x=700;
    public static final int y=800;
    public DrawinBoard() {
        super();
        this.setTitle("Fractal");
        this.setSize(x,y);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public void addFractal(Fractal f){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = f;
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
    }
    }
