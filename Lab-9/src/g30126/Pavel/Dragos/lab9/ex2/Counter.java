package g30126.Pavel.Dragos.lab9.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Counter extends JFrame implements ActionListener {

	JButton counter;
	JTextField txt;
	int count = 0;

	public Counter() {
		setTitle("Counter");
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initializare();
		setVisible(true);
	}

	void initializare() {
		this.setLayout(null);
		counter = new JButton("Counter");
		counter.setBounds(20, 20, 80, 20);
		counter.addActionListener(this);

		txt = new JTextField("0");
		txt.setBounds(102, 20, 80, 20);
		add(counter);
		add(txt);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		++count;
		txt.setText(count + " ");
	}

	public static void main(String[] args) {
		Counter c = new Counter();
	}

}

