package g30126.Pavel.Dragos.lab9.ex4;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class X0Game extends JFrame implements ActionListener {
	int[][] matrix = new int[3][3];
	int ok=-1;
	JButton jb1;
	JButton jb2;
	JButton jb3;
	JButton jb4;
	JButton jb5;
	JButton jb6;
	JButton jb7;
	JButton jb8;
	JButton jb9;
	int nr = 0;
	int counter = 0;

	public X0Game() {
		setTitle("X&0 Game");
		setDefaultCloseOperation(X0Game.EXIT_ON_CLOSE);
		setSize(300, 300);
		initialize();
		setVisible(true);
		for ( int i = 0; i < 3; i++)
			for ( int k = 0; k < 3; k++)
				matrix[i][k] = -1;

	}
	class Options extends JFrame implements ActionListener{
		JButton jb1;
		JButton jb2;
		JLabel jl ;
		public Options() {
			setTitle("Options");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setSize(400,200);
			initialize();
			setVisible(true);

		}
		public void initialize() {
			this.setLayout(null);
			if(ok==0)
				jl = new JLabel("Winner is 0!");
			else
				jl = new JLabel("Winner is X!");
			jl.setBounds(160, 20, 100, 30);
			jb1 = new JButton("New Game");
			jb1.addActionListener(this);
			jb1.setBounds(100, 60, 100, 25);

			jb2 = new JButton("Close");
			jb2.addActionListener(this);
			jb2.setBounds(200, 60, 100, 25);

			add(jb1);
			add(jb2);
			add(jl);

		}
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource().equals(jb1)) {
				new X0Game();
			}
			if(e.getSource().equals(jb2)) {
				System.exit(0);
			}
		}

	}

	public void initialize() {
		this.setLayout(null);
		jb1 = new JButton();
		jb1.setBounds(30, 30, 50, 50);
		jb1.addActionListener(this);

		jb2 = new JButton();
		jb2.setBounds(80, 30, 50, 50);
		jb2.addActionListener(this);

		jb3 = new JButton();
		jb3.setBounds(130, 30, 50, 50);
		jb3.addActionListener(this);

		jb4 = new JButton();
		jb4.setBounds(30, 80, 50, 50);
		jb4.addActionListener(this);

		jb5 = new JButton();
		jb5.setBounds(80, 80, 50, 50);
		jb5.addActionListener(this);

		jb6 = new JButton();
		jb6.setBounds(130, 80, 50, 50);
		jb6.addActionListener(this);

		jb7 = new JButton();
		jb7.setBounds(30, 130, 50, 50);
		jb7.addActionListener(this);

		jb8 = new JButton();
		jb8.setBounds(80, 130, 50, 50);
		jb8.addActionListener(this);

		jb9 = new JButton();
		jb9.setBounds(130, 130, 50, 50);
		jb9.addActionListener(this);

		add(jb1);
		add(jb2);
		add(jb3);
		add(jb4);
		add(jb5);
		add(jb6);
		add(jb7);
		add(jb8);
		add(jb9);
	}

	public int getLineWinner(int[][] m, int i) {
		for(int j = 0; j<3;j++) {
			if(j<2) {
				if(m[i][j]!=m[i][j+1])
				{
					return 0;}
			}
			else if(m[i][j-1]!=m[i][j])
			{
				return 0;
			}
		}
		return 1;
	}
	public int getColoneWinner(int[][] m, int j) {
		for(int i = 0; i<3;i++) {
			if(i<2){
			if(m[i][j]!=m[i+1][j]) {
				return 0;
			}
			}
			else if(m[i-1][j]!=m[i][j])
				return 0;
		}
		return 1;

	}
	public int getPrimaryDiagonalWinner(int[][] m) {
		int i;
		int[] v = new int[3];
		for( i = 0; i<3;i++) {
			v[i]=m[i][i];
			if(i<2){
			if(m[i][i]!=m[i+1][i+1] ) {
				return 0;
			}
			}
			else if(m[i-1][i-1]!=m[i][i])
				return 0;
		}
		for(i=0;i<3;i++)
			if(v[i]==-1)
				return 0;
		return 1;


	}
	public int getSecondaryDiagonalWinner(int[][] m) {
		int i;
		int[] v = new int[3];
		for( i = 0; i<3;i++) {
			v[i]=m[i][2-i];
			if(i<2){
			if(m[i][2-i]!=m[i+1][2-i-1]  ) {
				return 0;
			}
			}
			else if(m[i-1][2-i+1]!=m[i][2-i])
				return 0;
		}
		for(i=0;i<3;i++)
			if(v[i]==-1)
				return 0;
		return 1;
	}
	void addInMatrix(int[][] m, int i, int j, boolean x, boolean o) {
		if(x)
			m[i][j]=1;
		else if(o)
			m[i][j]=0;

	}
	@Override
	public void actionPerformed(ActionEvent ae) {
		Font font = new Font("Bell TM", 1000, 24);
		ArrayList<JButton> jb = new ArrayList<>();
		jb.add(jb1);
		jb.add(jb2);
		jb.add(jb3);
		jb.add(jb4);
		jb.add(jb5);
		jb.add(jb6);
		jb.add(jb7);
		jb.add(jb8);
		jb.add(jb9);
		int i;
		for (JButton j : jb) 
		{		
			if (counter == 0 && ae.getSource().equals(j)) {
				i = jb.indexOf(j);
				if(i>=0 && i<=2)
				{
					addInMatrix(matrix,0,i,true,false);
					if( getLineWinner(matrix,0)==1 || getColoneWinner(matrix,i)==1 || getPrimaryDiagonalWinner(matrix)==1 || getSecondaryDiagonalWinner(matrix)==1 )
					{	
						ok=1;
						new Options();
						X0Game.this.setEnabled(false);
					}
				}
				else if(i>2 && i<=5)
				{
					addInMatrix(matrix,1,i-3,true,false);
					if(getLineWinner(matrix,1)==1 || getColoneWinner(matrix,i-3)==1 || getPrimaryDiagonalWinner(matrix)==1 || getSecondaryDiagonalWinner(matrix)==1 )
					{	
						ok=1;
						new Options();
						X0Game.this.setEnabled(false);
					}
				}
				else if(i>5 && i<=8)
				{
					addInMatrix(matrix,2,i-6,true,false);
					if(getLineWinner(matrix,2)==1 || getColoneWinner(matrix,i-6)==1 || getPrimaryDiagonalWinner(matrix)==1 || getSecondaryDiagonalWinner(matrix)==1)
					{
						ok=1;
						new Options();
						X0Game.this.setEnabled(false);
					}
				}
				j.setFont(font);
				j.setText("X");
				counter++;
				j.setEnabled(false);
			} else if (counter == 1 && ae.getSource().equals(j)) {
				i = jb.indexOf(j);
				if(i>=0 && i<=2)
				{
					addInMatrix(matrix,0,i,false,true);
					if(getLineWinner(matrix,0)==1 || getColoneWinner(matrix,i)==1 || getPrimaryDiagonalWinner(matrix)==1 || getSecondaryDiagonalWinner(matrix)==1 )
					{	
						ok=0;
						new Options();
						X0Game.this.setEnabled(false);
					}
				}

				else if(i>2 && i<=5)
				{
					addInMatrix(matrix,1,i-3,false,true);
					if( getLineWinner(matrix,1)==1 || getColoneWinner(matrix,i-3)==1 || getPrimaryDiagonalWinner(matrix)==1 || getSecondaryDiagonalWinner(matrix)==1)
					{
						ok=0;
						new Options();
						X0Game.this.setEnabled(false);
					}
				}
				else if(i>5 && i<=8)
				{
					addInMatrix(matrix,2,i-6,false,true);
					if( getLineWinner(matrix,2)==1 || getColoneWinner(matrix,i-6)==1 || getPrimaryDiagonalWinner(matrix)==1 || getSecondaryDiagonalWinner(matrix)==1)
					{
						ok=0;
						new Options();
						X0Game.this.setEnabled(false);
					}
				}
				j.setFont(font);
				j.setText("0");
				counter = 0;
				j.setEnabled(false);
			}
		}		

	}
	void afisareMatrice() {
		for ( int i = 0; i < 3; i++)
		{for ( int k = 0; k < 3; k++)
			System.out.print(matrix[i][k]+" ");
		System.out.println();
		}
	}
	public static void main(String[] args) {
		X0Game xo = new X0Game();
	}
}