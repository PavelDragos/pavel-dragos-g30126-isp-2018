package g30126.Pavel.Dragos.lab9.ex3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FileContent extends JFrame implements ActionListener {
	JButton jbutton;
	JTextField jtextfield;
	JTextArea jtextarea;

	public FileContent() {
		setTitle("Counter");
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initialize();
		setVisible(true);
	}

	void initialize() {
		this.setLayout(null);
		jbutton = new JButton("Load");
		jbutton.setBounds(20, 20, 100, 20);
		jbutton.addActionListener(this);

		jtextfield = new JTextField();
		jtextfield.setBounds(125, 20, 200, 20);

		jtextarea = new JTextArea();
		jtextarea.setBounds(125, 50, 200, 100);

		add(jbutton);
		add(jtextfield);
		add(jtextarea);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		try {
			String filename = new String();
			filename = FileContent.this.jtextfield.getText();
			System.out.println(filename);
			BufferedReader in = null;
			in = new BufferedReader(new FileReader(filename));
			String s = new String();
			String s1;
			s1 = in.readLine();
			System.out.println(s1);
			while (s1 != null) {
				s += s1 + "\n";
				s1 = in.readLine();
			}
			System.out.println(s);
			in.close();
			FileContent.this.jtextarea.append(s);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		FileContent f = new FileContent();
	}
}